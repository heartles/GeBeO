import * as Discord from 'discord.js';
import * as Commando from 'discord.js-commando';
import * as winston from 'winston';
import {Sound} from 'utils/sounds';

module.exports = function(client: Commando.CommandoClient) {
    client.on('voiceStateUpdate', (oldMember: Discord.GuildMember, newMember: Discord.GuildMember) => {        
        if (oldMember.voiceChannelID == newMember.voiceChannelID) {
            return;
        }

        if (oldMember.voiceChannel != null &&
            oldMember.voiceChannel.members.size == 1 &&
            Sound.chanHasBot(oldMember.voiceChannel)) {
            
            winston.debug('clear queue since no one is listening')
            Sound.clearQueue(oldMember.voiceChannel);
        }
    });
}