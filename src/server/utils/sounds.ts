import * as Discord from 'discord.js';
import * as fs from 'fs';
var youtubedl = require('@microlink/youtube-dl')
import * as fh from './file_helper';
import * as winston from 'winston';
import {Bot, BotPool} from './bot';
import { EventEmitter } from 'events';
import * as AsyncLock from 'async-lock';

let lock: AsyncLock = new AsyncLock();

enum SoundType {
  YouTube,
  File,
}

class SoundItem {
  constructor(public name: string,
              public loc: any,
              public soundType: SoundType,
              public embed: Discord.RichEmbed,
              public timeStamp?: number) {}
}

interface ChannelQueue {
  /* Represents a voice channel and its queue.*/
  voiceChannel: Discord.VoiceChannel;
  queue: SoundItem[];
  bot: Bot;
  playing: SoundItem;
  dispatcher: Discord.StreamDispatcher;
}

export class _Sound extends EventEmitter{
  private queueDict: Map<string, ChannelQueue>;  // dict of ChannelQueues. 

  constructor(){
    super();
    this.queueDict = new Map();
  }


  private playNext = async (voiceChannel: Discord.VoiceChannel): Promise<void> => {
    winston.debug('playing the next sound');
    /* Play the next sound in the voice channel. */
    let cQueue: ChannelQueue = this.queueDict.get(voiceChannel.id);
    if (cQueue.queue.length == 0) {
      winston.debug('queue is empty');
      this.emit('queue update', null, null, voiceChannel.id);

      await lock.acquire(voiceChannel.id, () => {
        // No more sounds in queue.
        // Potential race condition if another sound gets queued up when code reaches this point.
        if(cQueue.bot) {
          winston.debug('disconnecting bot');
          cQueue.bot.disconnect(voiceChannel.id);
        }
        // Delete this cQueue from the dict.
        this.queueDict.delete(voiceChannel.id);
      })
      return;
    }
    await lock.acquire(voiceChannel.id, async () => {
      if(!cQueue.bot) {
        winston.debug('adding bot to channel');
        // If no bot is already picking up this queue.
        cQueue.bot = BotPool.getNextBot(voiceChannel);
        if(!cQueue.bot) {
          throw 'No bot is available';
        }
        await cQueue.bot.connect(voiceChannel.id);
      }  
    })

    // Pop first sound item from queue and play it.
    cQueue.playing = cQueue.queue.shift();
    cQueue.dispatcher = cQueue.bot.play(voiceChannel.id, cQueue.playing.loc, cQueue.playing.timeStamp || 0);

    this.emit('queue update', cQueue.queue, cQueue.playing, cQueue.voiceChannel.id);

    // Setup a callback for when this sound finishes playing.
    cQueue.dispatcher.on('end', () => {
      this.playNext(voiceChannel);
    });

    cQueue.dispatcher.on('error', (err) => {
      winston.info(err);
      this.playNext(voiceChannel);
    })
  }


  getQueue = (voiceChannelID: string): SoundItem[] => {
    let cQueue = this.queueDict.get(voiceChannelID);
    if (cQueue) {
      return cQueue.queue;
    } else {
      return null;
    }
  }


  getPlaying = (voiceChannelID: string): SoundItem => {
    let cQueue = this.queueDict.get(voiceChannelID);
    if (cQueue) {
      return cQueue.playing;
    } else {
      return null;
    }
  }


  chanHasBot = (voiceChannel: Discord.VoiceChannel): boolean => {
    if (this.queueDict.get(voiceChannel.id)) {
      return true;
    }
    return false;
  }


  skipSound = (voiceChannel: Discord.VoiceChannel, numSkip = 0): void => {
    if (numSkip == 0) {
      this.queueDict.get(voiceChannel.id).dispatcher.end();
    } else {
      if (this.queueDict.get(voiceChannel.id).queue.length < numSkip) {
        throw 'can\'t skip a position that\'s higher than the queue!';
      }
      this.queueDict.get(voiceChannel.id).queue.splice(numSkip - 1, 1);
      this.emit('queue update', this.queueDict.get(voiceChannel.id).queue, this.queueDict.get(voiceChannel.id).playing, voiceChannel.id);
    }
  }


  clearQueue = (voiceChannel: Discord.VoiceChannel): void => {
    let cQueue: ChannelQueue = this.queueDict.get(voiceChannel.id);
    cQueue.queue = [];
    this.skipSound(voiceChannel);
  }

  getQueueMessage = (voiceChannel: Discord.VoiceChannel): string => {
    let message: string = '';
    let counter: number = 1;
    let cQueue: ChannelQueue = this.queueDict.get(voiceChannel.id);
    message += 'Playing: ' + cQueue.playing.name + '\n';
    for (let i = 0; i < cQueue.queue.length; i++) {
      if (cQueue.queue[i]) {
        message += '#' + counter + ': ' + cQueue.queue[i].name + '\n';
        counter++;  
      }
    }
    return message;
  }


  queueSound = async (soundInput: string, voiceChannel: Discord.VoiceChannel, next=false, respMsg?: Discord.Message): Promise<void> => {
    /* Places sound in the prequeue, to be queued up internally.*/
    let soundItem: SoundItem;
    try{
      soundItem = await this.parseSoundInput(soundInput, voiceChannel);;
      if (respMsg) {
        await respMsg.edit({embed: soundItem.embed});
      }
    }
    catch(err) {
      console.log(err);
      // Something happened while parsing the sound input.
      throw err;
    }
    lock.acquire(voiceChannel.id, () => {
      if (this.queueDict.has(voiceChannel.id)){
        // Channel has a queue already.
        if(next) {
          // If sound is being queued for next position.
          this.queueDict.get(voiceChannel.id).queue.unshift(soundItem);
        }
        else {
          this.queueDict.get(voiceChannel.id).queue.push(soundItem);
        }
  
        this.emit('queue update', this.queueDict.get(voiceChannel.id).queue, this.queueDict.get(voiceChannel.id).playing, voiceChannel.id);
      }
      else {
        // Channel doesn't already have a queue.
        // Create a new sound queue and channel queue.
        let soundQueue: SoundItem[] = [soundItem];
        let channelQueue: ChannelQueue = {
          voiceChannel: voiceChannel,
          queue: soundQueue,
          bot: null,
          playing: null,
          dispatcher: null
        }
        this.queueDict.set(voiceChannel.id, channelQueue);
        this.playNext(voiceChannel);
      }  
    })
  };


  private parseSoundInput = async (soundInput: string, voiceChannel: Discord.VoiceChannel): Promise<SoundItem> => {
    let soundItem: SoundItem = new SoundItem(null, null, null, null);

    try {
      let filePath: string = fh.getFile(soundInput, voiceChannel.guild.id, fh.FileType.Sound);
      if (!fs.existsSync(filePath)) {
        throw "Doesn't exist";
      }
      soundItem.loc = filePath;
      soundItem.name = soundInput;
      soundItem.soundType = SoundType.File;
      soundItem.embed = new Discord.RichEmbed();
      soundItem.embed.setColor([218, 221, 51]);
      soundItem.embed.setAuthor('Sound');
      soundItem.embed.setTitle(soundInput);
      return soundItem;
    } catch (err) {
      console.log(err);
      // getFile failed, not a file sound.
      // Code continues.
    }

    // Check if is a valid youtube-dl link.
    try {
      let info: any = await new Promise((resolve, reject) => {
        youtubedl.getInfo(soundInput, (err: any, info: any) => {
          if (err) {
            reject(err);
          }
          resolve(info);
        })
      })
      console.log(info);
      soundItem.soundType = SoundType.YouTube;
      soundItem.name = info.title;
      soundItem.embed = new Discord.RichEmbed();
      soundItem.embed.setColor([218, 221, 51]);
      soundItem.embed.setAuthor('Link');
      soundItem.embed.setTitle(info.title);
      soundItem.embed.setURL(info.webpage_url);
      soundItem.embed.setThumbnail(info.thumbnail);
      soundItem.embed.setDescription(info.uploader);
      soundItem.loc = info.url
      try {
        soundItem.timeStamp = this.parseTimeStamp(soundInput);
      }
      catch (err) {
        console.log(err);
        console.log("Error with parseTimeStamp:\n" + err.stack);
        soundItem.timeStamp = 0;
      }
      return soundItem;
    } catch (err) {
      console.log(err);
      // youtube-dl throws an error.
      throw 'Invalid sound file or link!';
    }
  }


  private parseTimeStamp = (link: string): number => {
    /* Parses a youtube link for time stamps.
     * Returns timestamp in seconds. Returns null if no timestamp was found.
     * Throws an error if something weird happens during processing.
     * (e.g. if youtube timestamp format changes)
     * */
    let match: any = /(t=|start=).*?(?=&|$)/.exec(link);
    if(match) {
      match = match[0];
    }
    else {
      return null;
    }

    let timeStamp: string = match.split('=')[1].toLowerCase();

    // Two possible timestamp format: 1h1m40s or 3700.
    let ret: number = 0;
    if(/h/.test(timeStamp)) {
      let hour: string;
      [hour, timeStamp] = timeStamp.split('h');
      ret += Number(hour) * 3600;
    }
    if(/m/.test(timeStamp)) {
      let minute: string;
      [minute, timeStamp] = timeStamp.split('m');
      ret += Number(minute) * 60;
    }
    // Remove trailing 's'.
    timeStamp = timeStamp.replace(/s/, '');
    ret += Number(timeStamp);
    return ret;
  }
}



// Singleton
export let Sound: _Sound = new _Sound();
