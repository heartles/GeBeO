
// Config class for role color randomization
// This is the structure of 'storage/guilds/GUILD_ID/randrolecolor.json'
export class RandRoleColorCfg {
    constructor() {
        this.colors = [];
        this.roles = [];
    }
    
	colors: number[];
	roles: string[];
}