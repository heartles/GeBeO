import * as Commando from 'discord.js-commando';
import * as Discord from 'discord.js';
import * as fh from 'utils/file_helper';
import * as fs from 'fs';
import {RandRoleColorCfg} from 'utils/shared_config';

const detailsString: string = `Every day at midnight, GeBeO will assign any roles on its role-list a color at random from its color-list. This command will allow you to add roles and colors.
\`<COMMAND>\` must be one of add-role, remove-role, add-color, remove-color.
\`<COLOR>\` must be the hex digits of a color value.`

module.exports = class RandRoleColorCommand extends Commando.Command {
  constructor(client: Commando.CommandoClient) {
    let commandInfo: any = {
      name: 'randrolecolor',
      group: 'admin',
      memberName: 'randrolecolor',
      description: 'Randomize the colors of certain roles daily.',
      userPermissions: ['ADMINISTRATOR'],
			argsType: 'single',
			format: '<COMMAND> <ROLE | COLOR>',
			details: detailsString,
			examples: [
				'`!randrolecolor add-role Rainbow Admin`',
				'`!randrolecolor remove-role Gray Admin`',
				'`!randrolecolor add-color FF00FF`',
				'`!randrolecolor remove-color 36FFD4`',
			],
    }
    super(client, commandInfo);
	}
	
	getRoleFromName(guild: Discord.Guild, roleName: string): Discord.Role {
		let role: Discord.Role = null;
		guild.roles.forEach((value: Discord.Role, key: string) => {
			if (value.name == roleName) {
				role = value;
			}
		})
		return role;
	}

  async run(msg: Commando.CommandMessage, arg: string): Promise<Discord.Message | Discord.Message[]> {    
    let argArr: string[] = arg.split(' ');
    if (argArr.length < 2) {
			return msg.reply('please consult the help for this command to see the format!');
		} else {
			let command: string = argArr[0];
			argArr.splice(0, 1);
			let argVal = argArr.join(' ');


			if (command == 'add-role') {
				let role = this.getRoleFromName(msg.guild, argVal);
				if (role == null) {
					return msg.reply('Can\'t find that role!');
				}
				return this.addRole(msg, role);
			} else if (command == 'remove-role') {
				let role = this.getRoleFromName(msg.guild, argVal);
				if (role == null) {
					return msg.reply('Can\'t find that role!');
				}
				return this.rmRole(msg, role);
			} else if (command == 'add-color') {
				return this.addColor(msg, argVal);
			} else if (command == 'remove-color') {
				return this.rmColor(msg, argVal);
			} else {
				return msg.reply('Invalid usage. Please consult the help for this command to see the format!');
			}
		}
	}

	async addRole(msg: Commando.CommandMessage, role: Discord.Role): Promise<Discord.Message | Discord.Message[]> {
		let roleFile: string = fh.getGuildDir(msg.guild.id) + 'randrolecolor.json';
		let roleCfg: RandRoleColorCfg = new RandRoleColorCfg();
		roleCfg.roles = [];
		if (fs.existsSync(roleFile)) {
			roleCfg = JSON.parse(fs.readFileSync(roleFile, 'utf8'));
		}

		if (roleCfg.roles.indexOf(role.id) !== -1) {
			return msg.reply('Role is already on color randomization list!');
		}

		roleCfg.roles.push(role.id);
		fs.writeFileSync(roleFile, JSON.stringify(roleCfg));

		return msg.reply('Added Role ' + role.name);
	}

	async rmRole(msg: Commando.CommandMessage, role: Discord.Role): Promise<Discord.Message | Discord.Message[]> {
		let roleFile: string = fh.getGuildDir(msg.guild.id) + 'randrolecolor.json';
		let roleCfg: RandRoleColorCfg = new RandRoleColorCfg();
		roleCfg.roles = [];
		if (fs.existsSync(roleFile)) {
			roleCfg = JSON.parse(fs.readFileSync(roleFile, 'utf8'));
		}

		let index: number = roleCfg.roles.indexOf(role.id);
		if (index == -1) {
			return msg.reply('That role is not on the color randomization list!')
		}
		
		roleCfg.roles.splice(index, 1);

		fs.writeFileSync(roleFile, JSON.stringify(roleCfg));

		return msg.reply('Removed Role ' + role.name);
	}

	async addColor(msg: Commando.CommandMessage, colorStr: string): Promise<Discord.Message | Discord.Message[]> {
		let roleFile: string = fh.getGuildDir(msg.guild.id) + 'randrolecolor.json';
		let roleCfg: RandRoleColorCfg = new RandRoleColorCfg();
		roleCfg.colors = [];
		if (fs.existsSync(roleFile)) {
			roleCfg = JSON.parse(fs.readFileSync(roleFile, 'utf8'));
		}

		let color: number = parseInt('0x' + colorStr);
		if (isNaN(color)) {
			return msg.reply('Could not obtain color hex from input');
		}

		if (roleCfg.colors.indexOf(color) !== -1) {
			return msg.reply('Color is already on randomization list!');
		}

		roleCfg.colors.push(color);
		fs.writeFileSync(roleFile, JSON.stringify(roleCfg));

		return msg.reply('Added color ' + colorStr);
	}

	async rmColor(msg: Commando.CommandMessage, colorStr: string): Promise<Discord.Message | Discord.Message[]> {
		let roleFile: string = fh.getGuildDir(msg.guild.id) + 'randrolecolor.json';
		let roleCfg: RandRoleColorCfg = new RandRoleColorCfg();
		roleCfg.roles = [];
		if (fs.existsSync(roleFile)) {
			roleCfg = JSON.parse(fs.readFileSync(roleFile, 'utf8'));
		}

		let color: number = parseInt('0x' + colorStr);
		if (isNaN(color)) {
			return msg.reply('Could not obtain color hex from input');
		}

		let index: number = roleCfg.colors.indexOf(color);
		if (index == -1) {
			return msg.reply('That color is not on the randomization list!')
		}
		
		roleCfg.colors.splice(index, 1);

		fs.writeFileSync(roleFile, JSON.stringify(roleCfg));

		return msg.reply('Removed color ' + colorStr);
	}
}
