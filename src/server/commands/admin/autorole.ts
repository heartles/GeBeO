import * as Commando from 'discord.js-commando';
import * as Discord from 'discord.js';
import * as fh from 'utils/file_helper';
import * as fs from 'fs';

const helpDocs: string = (
`Have GeBeO add a given role to any users that join the server.`
);

module.exports = class AutoRoleCommand extends Commando.Command {
  constructor(client: Commando.CommandoClient) {
    let commandInfo: any = {
      name: 'autorole',
      group: 'admin',
      memberName: 'autorole',
      description: 'Automatically give new users a role.',
      userPermissions: ['ADMINISTRATOR'],
      argsType: 'single',
      details: helpDocs,
      format: 'autorole <ROLE>'
    }
    super(client, commandInfo);

    client.on('guildMemberAdd', this.roleAdd);
  }

  async run(msg: Commando.CommandMessage, arg: string): Promise<Discord.Message | Discord.Message[]> {
    let role: Discord.Role = null;
    if (arg !== "") {
      msg.guild.roles.forEach((value: Discord.Role) => {
        if (value.name == arg) {
          role = value;
        }
      })

      if (role == null) {
        return msg.reply('Can\'t find that role!');
      }
    }

    (msg.guild as any).settings.set("autorole_id", role.id);

    return msg.reply(`Set automatic member role to ${role.name}!`);
  }

  async roleAdd(member: Discord.GuildMember) {
    let roleId = (member.guild as any).settings.get("autorole_id");
    let role: Discord.Role = member.guild.roles.find(
      r => r.id == roleId);

    if (role == null) {
      return;
    }

    return member.addRole(role, "New User AutoRole");
  }
}
