import * as Commando from 'discord.js-commando';
import * as Discord from 'discord.js';

module.exports = class DuplicateRoleCommand extends Commando.Command {
  constructor(client: Commando.CommandoClient) {
    let commandInfo: any = {
      name: 'duplicaterole',
      group: 'admin',
      memberName: 'duplicaterole',
      description: 'Give every user in the server that has a specific role, a second specific role.',
      userPermissions: ['ADMINISTRATOR'],
      argsType: 'single',
    }
    super(client, commandInfo);
  }

  async run(msg: Commando.CommandMessage, arg: string): Promise<Discord.Message | Discord.Message[]> {
    let roleStrings: String[] = arg.split('|');
    if (roleStrings.length < 2) {
      return msg.reply('please consult the help for this command to see the format!');
    } else {
      if (roleStrings[0] == roleStrings[1]) {
        return msg.channel.send('Those are the same roles, please select two different ones!');
      }

      let roles: Discord.Role[] = [null, null];
      msg.guild.roles.forEach((role: Discord.Role, key: string) => {
        let roleIndex = roleStrings.findIndex(str => str == role.name)
        if (roleIndex != -1) {
          roles[roleIndex] = role;
        }
      })

      if (!roles[0] || !roles[1]) {
        return msg.channel.send('Can\'t find one of those roles!');
      }

      let opMsg: Discord.Message = await msg.channel.send('Adding `' + roles[1].name + '` to everyone who has the role `' + roles[0].name + '`...') as Discord.Message;

      let guildMembers: Discord.GuildMember[] = msg.guild.members.array();
      for (let i = 0; i < guildMembers.length; i++) {
        if (guildMembers[i].roles.has(roles[0].id) && !guildMembers[i].roles.has(roles[1].id)) {
          await guildMembers[i].addRoles([roles[1]]);
        }
      }

      return opMsg.edit('Done!');
    }
  }
}
